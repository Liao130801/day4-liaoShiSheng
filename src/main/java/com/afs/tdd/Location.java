package com.afs.tdd;

public class Location {
    private int coordinateX;
    private Direction direction;
    private int coordinateY;
    public int getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }



    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    public int getCoordinateY() {
        return coordinateY;
    }


    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }



    public Location(int coordinateX, int coordinateY, Direction north) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.direction = north;
    }
}
