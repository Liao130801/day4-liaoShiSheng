package com.afs.tdd;

import java.util.List;

import static com.afs.tdd.Direction.*;

public class MarsRover {
    private Location location;
    private Command command;

    public MarsRover(Location location) {
        this.location = location;
    }

    public Location executeCommand(Command commandMove) {
        switch (commandMove) {
            case Move:
                move();
                break;
            case TurnLeft:
                turnLeft();
                break;
            case TurnRight:
                turnRight();
                break;
        }
        return location;
    }
    void move() {
        switch (location.getDirection()) {
            case North:
                location.setCoordinateY(location.getCoordinateY()+1);
                break;
            case Sounth:
                location.setCoordinateY(location.getCoordinateY()-1);
                break;
            case East:
                location.setCoordinateX(location.getCoordinateX()+1);
                break;
            case West:
                location.setCoordinateX(location.getCoordinateX()-1);
                break;

        }
    }
    void turnLeft() {
        switch (location.getDirection()) {
            case North:
                location.setDirection(West);
                break;
            case Sounth:
                location.setDirection(East);
                break;
            case East:
                location.setDirection(North);
                break;
            case West:
                location.setDirection(Sounth);
                break;
        }
    }

    void turnRight() {
        switch (location.getDirection()) {
            case North:
                location.setDirection(East);
                break;
            case Sounth:
                location.setDirection(West);
                break;
            case East:
                location.setDirection(Sounth);
                break;
            case West:
                location.setDirection(North);
                break;

        }
    }

    public Location executeBathCommand(List<Command> commandMove) {
        commandMove.stream().forEach(command->{
            executeCommand(command);
        });
        return location;
    }
}
