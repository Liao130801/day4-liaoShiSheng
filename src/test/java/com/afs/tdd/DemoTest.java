package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class DemoTest {
    @Test
    void should_change_to_0_1_North_when_executeCommand_given_location_0_0_N_and_command_move() {
         Location location = new Location(0,0,Direction.North);
         MarsRover marsRover  = new MarsRover(location);
         Command commandMove = Command.Move;
         Location locationAfterMove = marsRover.executeCommand(commandMove);

        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(1,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_N_and_command_move() {
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover  = new MarsRover(location);
        Command commandMove = Command.TurnLeft;
        Location locationAfterMove = marsRover.executeCommand(commandMove);

        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_N_and_command_move() {
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover  = new MarsRover(location);
        Command commandMove = Command.TurnRight;
        Location locationAfterMove = marsRover.executeCommand(commandMove);

        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_Sounth_when_executeCommand_given_location_0_1_S_and_command_move() {
        Location location = new Location(0,1,Direction.Sounth);
        MarsRover marsRover  = new MarsRover(location);
        Command commandMove = Command.Move;
        Location locationAfterMove = marsRover.executeCommand(commandMove);

        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.Sounth,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_East_when_executeCommand_given_location_0_0_S_and_command_move() {
        Location location = new Location(0,0,Direction.Sounth);
        MarsRover marsRover  = new MarsRover(location);
        Command commandMove = Command.TurnLeft;
        Location locationAfterMove = marsRover.executeCommand(commandMove);

        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_0_0_S_and_command_move() {
        Location location = new Location(0,0,Direction.Sounth);
        MarsRover marsRover  = new MarsRover(location);
        Command commandMove = Command.TurnRight;
        Location locationAfterMove = marsRover.executeCommand(commandMove);

        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_1_0_East_when_executeCommand_given_location_0_0_E_and_command_move() {
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover  = new MarsRover(location);
        Command commandMove = Command.Move;
        Location locationAfterMove = marsRover.executeCommand(commandMove);

        Assertions.assertEquals(1,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.East,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_E_and_command_move() {
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover  = new MarsRover(location);
        Command commandMove = Command.TurnLeft;
        Location locationAfterMove = marsRover.executeCommand(commandMove);

        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_Sounth_when_executeCommand_given_location_0_0_E_and_command_move() {
        Location location = new Location(0,0,Direction.East);
        MarsRover marsRover  = new MarsRover(location);
        Command commandMove = Command.TurnRight;
        Location locationAfterMove = marsRover.executeCommand(commandMove);

        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.Sounth,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_West_when_executeCommand_given_location_1_0_W_and_command_move() {
        Location location = new Location(1,0,Direction.West);
        MarsRover marsRover  = new MarsRover(location);
        Command commandMove = Command.Move;
        Location locationAfterMove = marsRover.executeCommand(commandMove);

        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_Sounth_when_executeCommand_given_location_0_0_W_and_command_move() {
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover  = new MarsRover(location);
        Command commandMove = Command.TurnLeft;
        Location locationAfterMove = marsRover.executeCommand(commandMove);

        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.Sounth,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_North_when_executeCommand_given_location_0_0_W_and_command_move() {
        Location location = new Location(0,0,Direction.West);
        MarsRover marsRover  = new MarsRover(location);
        Command commandMove = Command.TurnRight;
        Location locationAfterMove = marsRover.executeCommand(commandMove);

        Assertions.assertEquals(0,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(0,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.North,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_2_2_West_when_executeCommand_given_location_0_0_N_and_command_move() {
        Location location = new Location(0,0,Direction.North);
        MarsRover marsRover  = new MarsRover(location);
        List<Command> commandMove = new ArrayList<>();
        commandMove.add(Command.Move);
        commandMove.add(Command.Move);
        commandMove.add(Command.TurnRight);
        commandMove.add(Command.Move);
        commandMove.add(Command.Move);
        commandMove.add(Command.TurnRight);
        commandMove.add(Command.Move);
        commandMove.add(Command.TurnRight);
        commandMove.add(Command.TurnRight);
        commandMove.add(Command.Move);
        commandMove.add(Command.TurnLeft);
        Location locationAfterMove = marsRover.executeBathCommand(commandMove);

        Assertions.assertEquals(2,locationAfterMove.getCoordinateX());
        Assertions.assertEquals(2,locationAfterMove.getCoordinateY());
        Assertions.assertEquals(Direction.West,locationAfterMove.getDirection());
    }

}
