
###1.Key learning, skills and experience acquired
- 1). Today I had a code review and saw my classmates' code styles, and learnt more concise and clear naming and code writing styles. I think the code style should be simple and clear, and the whole team's style should be unified, I want to standardise my code more in the future.
- 2). Learned the observer pattern, the observer pattern is a very important design pattern, many open source libraries use this pattern. I learnt how to use it, and in the future I want to apply more design patterns to my daily development.
- 3). In the afternoon, I learnt about TDD, i.e. Test Driven Development. The development process is cumbersome, but can ensure that the programme will not appear a variety of unpredictable errors, very useful, I want to change my development method, using TDD way to apply to the project.
   
###2.Problem / Confusing / Difficulties

The problem encountered today is a change in the development method, temporarily not accustomed to the TDD development method, in the future to be used more to ensure the safe operation of the project


###3.Other Comments / Suggestion

I need to practice more in the future